/**
 * Diese Klasse modeliert einen Schauspieler
 *
 * @author Cedrico Knoesel
 * @since 21.11.2018
 */
public class Actor {
    //Attribute
    Agency agency; //Agentur unter der der Schauspieler unter Vertrag genommen ist
    String firstName;
    String lastName;
    int id; //neunstellige Identifikationsnummer

    //Geburtstag in Tag, Monat und Jahr:
    byte dayOfBirth;
    Months monthOfBirth;
    short yearOfBirth;

    Role role; //Rolle des Schauspielers
}
