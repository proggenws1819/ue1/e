/**
 * Diese Klasse modeliert eine Catchphrase, die zu einer Rolle gehört
 *
 * @author Cedrico Knoesel
 * @see Role
 * @since 21.11.2018
 */
public class Catchphrase {
    //Attribute
    String phrase;
    short id;
}
