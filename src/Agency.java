/**
 * Diese Klasse modeliert eine Schauspieler-Agentur
 *
 * @author Cedrico Knoesel
 * @see Actor
 * @since 21.11.2018
 */
public class Agency {
    //Attribute
    String name;
    int id; //achtstellige Identifikationsnummer
    int size; //in Quadratmeter
    float longitude;
    float latitude;
}
