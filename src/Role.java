/**
 * Diese Klasse modeliert eine Rolle, die von einem Schauspieler gespielt werden kann
 *
 * @author Cedrico Knoesel
 * @see Actor
 * @since 21.11.2018
 */
public class Role {
    //Attribute
    boolean mainRole; //Haupt- (true) oder Nebenrolle (false)
    String name;
    short id; //vierstellige Identifikationsnummer
    Catchphrase catchphrase;
}
